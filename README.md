# README #

Vending Machine project.

### What is this repository for? ###

This project has been created to demonstrate TDD and Unit Testing practices.

### How do I get set up? ###

You can download and modify the code as this project is just for demonstration purposes.

### Who do I talk to? ###

For any concerns about the code, please write to victorlsgr@gmail.com.