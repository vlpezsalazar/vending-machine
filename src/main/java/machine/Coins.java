package machine;

public enum Coins {
    POUND(1),
	EURO(0.6), 
	FIFTYPENCE(0.5),
	TWENTYPENCE(0.2),
	TENPENCE(0.1)
	
	;
	
	private double value;

	private Coins(double value){
		this.value = value;
	}

	public double getValue() {
		return value;
	}
}
