package machine;

public enum Item {
	ITEM_A(0.60),
	ITEM_B(1),
	ITEM_C(1.70),
	;

	private double cost;
	
	private Item(double cost){
		this.cost = cost;
	}

	public double getCost() {
		return cost;
	}
	
}
