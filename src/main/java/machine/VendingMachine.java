package machine;

import java.util.List;

public interface VendingMachine {

	void switchOff();

	void switchOn();
	
	boolean isMachineOn();

	double getCurrentlyInsertedAmount();

	void insertCoin(Coins cash);

	void reset();

	List<Coins> returnMoney();

	List<Coins> sellItem(Item itemA);

}
