package machine.exceptions;

import machine.Coins;

public class CoinNotAcceptedException extends RuntimeException {

	public CoinNotAcceptedException(Coins coinType) {
		super(String.format("The machine doesn't accept %s coins", coinType.name()));
	}
}
