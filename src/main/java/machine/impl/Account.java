package machine.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.EnumMap;
import java.util.EnumSet;
import java.util.List;
import java.util.Map.Entry;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import machine.Coins;

public class Account {
    private EnumSet<Coins> ACCEPTED_COINS;
    private EnumMap<Coins, Integer> cash;
    
    public Account(final EnumSet<Coins> acceptedCoins) {
        this.ACCEPTED_COINS = acceptedCoins;
        cash = new EnumMap<Coins, Integer>(Coins.class);
        resetAccount();
    }

    public double getAmount() {
        double currentAmount = 0;
        for (Entry<Coins, Integer> currentCoinEntry : cash.entrySet()){
            currentAmount += getAmountForCoin(currentCoinEntry);
        }

        return currentAmount;
    }
    public List<Coins> returnCoins() {
        ArrayList<Coins> coinsToReturn = new ArrayList<>();
        cash.forEach((x, y) -> repeat(y, () -> coinsToReturn.add(x)));
        resetAccount();
        return coinsToReturn;
    }
    public void addCoin(Coins coin) {
        cash.compute(coin, (key, numberOfCoins) -> numberOfCoins + 1);          
    }
    public void addAllCoinsFrom(Account account){
        cash.replaceAll((x, y) -> y + account.getNumberOfCoins(x));
    }
    public int getNumberOfCoins(Coins coin) {
        return cash.get(coin);
    }
    public List<Coins> getChangeFor(double changeToReturn) {
        final List<Coins> coinsToReturn = new ArrayList<Coins>();
        for (Entry<Coins, Integer> coinEntry : cash.entrySet()) {
            while (hasCoinsLeft(coinEntry)
                    && getCoinValue(coinEntry) <= changeToReturn) {
                decreaseNumberOfCoins(coinEntry);
                changeToReturn -= getCoinValue(coinEntry);
                coinsToReturn.add(coinEntry.getKey());
            }
        }
        return coinsToReturn;
    }
    void resetAccount() {
        getAcceptedCoins().forEach(x -> cash.put(x, 0));
    }

    private double getAmountForCoin(Entry<Coins, Integer> coinEntry){
        return getNumberOfCoins(coinEntry)*getCoinValue(coinEntry);
    }

    private Integer getNumberOfCoins(Entry<Coins, Integer> coinEntry) {
        return coinEntry.getValue();
    }

    private double getCoinValue(Entry<Coins, Integer> coinEntry) {
        return coinEntry.getKey().getValue();
    }
    private Stream<Coins> getAcceptedCoins() {
        return Arrays.stream(Coins.values()).filter(this::isCoinAccepted);
    }
    private boolean isCoinAccepted(Coins coin) {
        return ACCEPTED_COINS.contains(coin);
    }
    private void repeat(int count, Runnable action) {
        IntStream.range(0, count).forEach(i -> action.run());
    }

    private void decreaseNumberOfCoins(Entry<Coins, Integer> coinEntry) {
        coinEntry.setValue(coinEntry.getValue() - 1);
    }

    private boolean hasCoinsLeft(Entry<Coins, Integer> coinEntry) {
        return coinEntry.getValue() > 0;
    }
}
