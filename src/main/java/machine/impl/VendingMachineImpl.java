package machine.impl;

import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;

import machine.Coins;
import machine.Item;
import machine.VendingMachine;
import machine.exceptions.CoinNotAcceptedException;

public class VendingMachineImpl implements VendingMachine {

    private final EnumSet<Coins> ACCEPTED_COINS = EnumSet.of(Coins.TENPENCE,
            Coins.FIFTYPENCE, Coins.TWENTYPENCE, Coins.POUND);

    private Account currentInsertedMoney;

    private Account savedMoney;

    private boolean machineWorking;

    public VendingMachineImpl() {
        machineWorking = true;
        savedMoney = new Account(ACCEPTED_COINS);
        currentInsertedMoney = new Account(ACCEPTED_COINS);
    }

    public void switchOff() {
        machineWorking = false;
    }

    public void switchOn() {
        machineWorking = true;
    }

    public boolean isMachineOn() {
        return machineWorking;
    }

    public double getCurrentlyInsertedAmount() {
        if (!isMachineOn())
            throw new MachineIsOffException();
        return currentInsertedMoney.getAmount();
    }

    public void insertCoin(Coins coin) {
        if (!isMachineOn())
            throw new MachineIsOffException();
        if (!ACCEPTED_COINS.contains(coin))
            throw new CoinNotAcceptedException(coin);
        currentInsertedMoney.addCoin(coin);
    }

    public List<Coins> returnMoney() {
        if (!isMachineOn())
            throw new MachineIsOffException();
        return currentInsertedMoney.returnCoins();
    }

    @Override
    public List<Coins> sellItem(Item itemA) {
        if (!isMachineOn())
            throw new MachineIsOffException();
        List<Coins> coinsToReturn = new ArrayList<Coins>();
        double changeToReturn = getCurrentlyInsertedAmount() - itemA.getCost();
        if (changeToReturn > 0) {
            saveMoney();
            coinsToReturn = savedMoney.getChangeFor(changeToReturn);
        }
        return coinsToReturn;
    }

    public void reset() {
        currentInsertedMoney.resetAccount();
        savedMoney.resetAccount();
    }

    private void saveMoney() {
        savedMoney.addAllCoinsFrom(currentInsertedMoney);
        currentInsertedMoney.resetAccount();
    }

}
