package machine;

import static org.hamcrest.core.Is.is;
import static org.hamcrest.core.IsCollectionContaining.hasItems;
import static org.junit.Assert.assertThat;

import java.util.EnumSet;

import machine.impl.Account;

import org.junit.Before;
import org.junit.Test;

public class AccountTest {

    private Account account;
    
    @Before
    public void setUp(){
        account = new Account(EnumSet.allOf(Coins.class));
    }
    
    
    @Test
    public void shouldReturnTheNumberOfCoinsAdded() {
        account.addCoin(Coins.EURO);
        
        assertThat(account.getNumberOfCoins(Coins.EURO), is(1));
    }
    @Test
    public void shouldReturnCoinsAdded() {
        account.addCoin(Coins.EURO);
        account.addCoin(Coins.FIFTYPENCE);
        account.addCoin(Coins.TENPENCE);
        
        assertThat(account.returnCoins(), hasItems(Coins.EURO, Coins.FIFTYPENCE, Coins.TENPENCE));
    }
    
    
    @Test
    public void shouldReturnCoinsAddedFromOtherAccount() {
        Account account2 = new Account(EnumSet.allOf(Coins.class));
        account2.addCoin(Coins.EURO);
        account2.addCoin(Coins.FIFTYPENCE);
        account2.addCoin(Coins.TENPENCE);
        
        account.addAllCoinsFrom(account2);
      
        
        assertThat(account.returnCoins(), hasItems(Coins.EURO, Coins.FIFTYPENCE, Coins.TENPENCE));
    }
    @Test
    public void shouldReturnTheLeastPossibleCoins() {
        account.addCoin(Coins.POUND);
        account.addCoin(Coins.EURO);
        account.addCoin(Coins.EURO);
        account.addCoin(Coins.FIFTYPENCE);
        account.addCoin(Coins.TENPENCE);
        account.addCoin(Coins.TWENTYPENCE);
        account.addCoin(Coins.TWENTYPENCE);

        assertThat("The account has not returned the expected coins.",
            account.getChangeFor(1.4),
            hasItems(Coins.POUND, Coins.TWENTYPENCE, Coins.TWENTYPENCE));
    }

    
}
