package machine;

import machine.impl.VendingMachineImpl;

import com.google.inject.AbstractModule;

public class ConfigureDependencies extends AbstractModule {

	@Override
	protected void configure() {
		bind(VendingMachine.class).to(VendingMachineImpl.class);

	}

}
