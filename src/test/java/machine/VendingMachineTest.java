package machine;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import machine.exceptions.CoinNotAcceptedException;
import machine.impl.MachineIsOffException;

import org.hamcrest.core.IsCollectionContaining;
import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import com.google.inject.Guice;
import com.google.inject.Injector;

public class VendingMachineTest {

    private VendingMachine machine;

    private static Injector injector;

    @BeforeClass
    public static void setUpBeforeClass() throws Exception {
        injector = Guice.createInjector(new ConfigureDependencies());

    }

    @Before
    public void setUp() {
        machine = injector.getInstance(VendingMachine.class);
    }

    @After
    public void restore() {
        machine.reset();
    }

    @Test(expected = MachineIsOffException.class)
    public void shouldNotSellItemsWhenTheMachineIsOff() {
        machine.switchOff();
        machine.sellItem(Item.ITEM_A);
        fail("the sellItem operation has been executed while the machine was off");

    }

    @Test
    public void shouldAcceptTwentyPence() {
        machine.insertCoin(Coins.TWENTYPENCE);
        assertEquals("The machine should be accepting 20c coins.",
                Coins.TWENTYPENCE.getValue(),
                machine.getCurrentlyInsertedAmount(), 0);
    }

    @Test
    public void shouldAcceptAPound() {
        machine.insertCoin(Coins.POUND);
        assertEquals("The machine should be accepting 1 pounds coins.",
                Coins.POUND.getValue(), machine.getCurrentlyInsertedAmount(), 0);
    }

    @Test(expected = CoinNotAcceptedException.class)
    public void shouldRejectForeignCoins() {
        machine.insertCoin(Coins.EURO);
        fail("The machine should not be accepting foreign coins.");
    }

    @Test
    public void shouldReturnCurrentlyInsertedMoneyWhenReturnMoneyIsPressed() {
        machine.insertCoin(Coins.POUND);
        machine.insertCoin(Coins.FIFTYPENCE);
        machine.insertCoin(Coins.TWENTYPENCE);
        machine.insertCoin(Coins.TWENTYPENCE);

        assertThat("The machine has not returned the expected coins.",
                machine.returnMoney(), IsCollectionContaining.hasItems(
                        Coins.POUND, Coins.FIFTYPENCE, Coins.TWENTYPENCE,
                        Coins.TWENTYPENCE));
    }

    @Test
    public void shouldReturnChangeBackWhenInsertedMoreThanPriceCostForAnItem() {

        machine.insertCoin(Coins.POUND);
        machine.insertCoin(Coins.FIFTYPENCE);
        machine.insertCoin(Coins.TWENTYPENCE);
        machine.insertCoin(Coins.TWENTYPENCE);

        assertThat("The machine has not returned the expected coins.",
                machine.sellItem(Item.ITEM_A),
                IsCollectionContaining.hasItems(Coins.POUND, Coins.TWENTYPENCE));
    }

}
